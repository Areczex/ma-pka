#include <iostream>
#include <conio.h.>
#include <time.h>
#include <cstdlib>
#include <Windows.h>
#include <cmath>

using namespace std;

struct pola
{
	char malpa;
	bool czy;
	int pX, X, pY, Y;


};

class krolik
{
private:
	pola dane;

	char plansza[40][40];

public:

	krolik()
	{

	}

	krolik(int x, int y)
	{
		dane.pX = 0;
		dane.pY = 0;
		dane.malpa = '@';
		dane.czy = false;
		dane.X = x;
		dane.Y = y;
		inicjalizujPlansze(dane.X, dane.Y);

	}

	void inicjalizujPlansze(int x, int y)
	{
		for (int i = 0; i < 40; i++)
		{
			for (int j = 0; j < 40; j++)
			{
				plansza[i][j] = ' ';
			}
		}
		plansza[x][y] = dane.malpa;

	}

	void wyswietlPlansze()
	{
		for (int i = 0; i < 40; i++)
		{
			for (int j = 0; j < 40; j++)
			{
				cout << plansza[i][j];
			}
			cout << endl;
		}

	}

	void losuj()
	{
		if (dane.czy == true)
		{
			dane.pY = dane.Y;
			dane.pX = dane.X;
			dane.X = abs((dane.pX + rand() % 3 - 1) % 40);
			dane.Y = abs((dane.pY + rand() % 3 - 1) % 40);
		}
		else
		{
			dane.pY = dane.Y;
			dane.pX = dane.X;
			dane.czy = true;
		}

	}

	void steruj()
	{
		wyswietlPlansze();
		system("cls");

		while (true)
		{
			losuj();
			inicjalizujPlansze(dane.X, dane.Y);
			wyswietlPlansze();
			Sleep(100);
			system("cls");
		}
	}

};

int main()
{
	srand(time(NULL));
	krolik krolik(0, 0);
	krolik.steruj();

	system("pause");
	return 0;
}